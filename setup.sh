#!/bin/bash

[[ "$(id -u)" != "0" ]] && echo "This script must be run as root" && exit 1

echo ">> Setting up symbolic links"
set -x
mkdir -p /usr/share/manjaro-arm-tools/lib
mkdir -p /var/lib/manjaro-arm-tools/{pkg,tmp,img}
mkdir -p /var/cache/manjaro-arm-tools/{img,pkg}
mkdir -p /var/lib/manjaro-arm-tools/pkg/aarch64/build
mkdir -p /etc/manjaro-arm-tools

ln -sf $PWD/lib/* /usr/share/manjaro-arm-tools/lib/
ln -sf $PWD/lib/manjaro-arm-tools.conf /etc/manjaro-arm-tools/manjaro-arm-tools.conf
ln -sf $PWD/bin/* /usr/bin/
set +x
echo ">> Setup manjaro-arm-tools"

systemctl start systemd-binfmt.service

echo ">> Started binfmt"
